/*Optional take home work/activity for tomorrow:


Create a fully-functioning login page that will display a sweetalert window with a success message if the user logs in sucessfully (and also sets all input values to an empty string), or a sweetalert window with an error message if the user does not.

Do NOT create a new component for this. All code must be added to this page.

Import this page to App.js and comment our Register component to test.


*/


import { Form, Button, Container } from 'react-bootstrap'
import { useState, useEffect} from 'react'
import Swal from 'sweetalert2'



export default function LogIn() {

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [isActive, setIsActive] = useState(false)

useEffect(() => {
	if(email !== "" && password !== ""){
		setIsActive(true)
	}else{
		setIsActive(false)
	}

},[email, password]) 

const loginUser = (e) => {
		e.preventDefault()

		fetch("http://localhost:4000/users/login", {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email : email,
				password : password
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
					Swal.fire({
						title: "Logged in succesfully!",
						icon : "success",
						text: "You have been succesfully logged in!"
					})

						setEmail("")
						setPassword("")

				}else{
					Swal.fire({
						title: "Log in failed",
						icon : "error",
						text: "You have not been logged in"



				})
			}
		})
	}

	return (
		<Container>
			<Form className="mt-3" onSubmit={e => loginUser(e)} >
			  <Form.Group controlId="formBasicEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control type="email" placeholder="Enter email" required value={email} onChange={e => setEmail(e.target.value)}/>
			  </Form.Group>

			  <Form.Group controlId="formBasicPassword">
			    <Form.Label>Password</Form.Label>
			    <Form.Control type="password" placeholder="Password" required value={password} onChange={e => setPassword(e.target.value)}/>
			  </Form.Group>
			  {(isActive)
						?
						<Button variant="primary" type="submit">Login</Button>
						:
						<Button variant="primary" disabled>Login</Button>
				}
			</Form>	
		</Container>
	)
}

