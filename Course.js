/*
	In this file, first import Card and Button from the react-bootstrap
	Create a Course component with the following features:
	Inside the Card Component, create a Card.Body component,
	Inside the Card.Body component, create a  Card.Title component with a name of sample course (use any course name)
	Inside the Card.Body component, create a Card.Text component
	Inside the Card.Text component, create an h3 component with the word "Description"
	Inside the Card.Text component, create a p component with any place holder text below the h3 component
	Inside the Card.Text component, create an h4 component with the word "Price" below the p component
	Inside the Card.Text component, create a p component with any price
	Inside the Card.Text component, create a Button component with a variant aattribute with a value of "primary" that says "Enroll"
	Import the created Course Component in App.js. Comment out the Home component and add the Course component in the return statement
	Make sure to check your output
	Time limit 40 mins: including push time 
*/


import { Row, Col, Card, Button } from 'react-bootstrap'


export default function Course() {
	return(
		<Row>
			<Card>
				<Card.Body>
					<Card.Title>Basic Java Script</Card.Title>
					<Card.Text>
						<h3>Description</h3>
						<p>Learn Basic Java Script</p>
						<h4>Price</h4>
						<p>5,000</p>
						<Button class="btn-primary">Enroll</Button>
					</Card.Text>
				</Card.Body>
			</Card>
		</Row>

	)
}
